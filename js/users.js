$("#add-user").on("click", () => {
    let login = $("#login").val().trim().toLowerCase()
    let password = $("#password").val().trim()
    let role = $("#role").val()
    let errors = []
    if (login == "") {
        errors.push("Не указан логин")
    } else if (password == "") {
        errors.push("Не указан пароль")
    }
    if (errors.length > 0) {
        alert(errors[0])
    } else {
        $.post("/api/addUser.php", { login, password, role }, data => {
            data = JSON.parse(data)
            if (data.success) {
                alert("Пользователь " + login + " успешно добавлен")
                location.reload()
            } else {
                alert(data.error)
            }
        })
    }
})

$(".remove-user").on("click", function() {
    let login = $(this).data("login")
    if (confirm("Удалить пользователя " + login + "?")) {
        $.post("/api/removeUser.php", { login }, data => {
            data = JSON.parse(data)
            if (data.success) {
                alert("Пользователь " + login + " успешно удалён")
                location.reload()
            } else {
                alert(data.error)
            }
        })
    }
})
