<?
    require "db.php";

    if (!isset($_SESSION["user"])) {
        header("Location: /");
    }

    $data = $_POST;
    $errors = [];

    if (isset($data["change_pass"])) {
        if (trim(($data["old_password"])) == "") {
            $errors[] = "Не указан старый пароль";
        }

        if (trim($data["new_password"]) == "") {
            $errors[] = "Не указан новый пароль";
        }

        if (trim($data["repeat_password"]) == "") {
            $errors[] = "Не указан повтор пароля";
        }

        if (trim($data["new_password"]) != trim($data["repeat_password"])) {
            $errors[] = "Новый пароль и повтор не совпадают";
        }

        if (empty($errors)) {
            $user = R::findOne("users", "login = ?", [$_SESSION["user"]->login]);
            if (password_verify($data["old_password"], $user->password)) {
                $user->password = password_hash($data["new_password"], PASSWORD_DEFAULT);
                R::store($user);
            } else {
                $errors[] = "Неверно указан старый пароль";
            }
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Библиотека</title>
        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/login.css">
    </head>
    <body>
        <header>
            <ul>
                <li><a href="/">Главная</a></li>
                <?
                    if (isset($_SESSION["user"])) {
                        if ($_SESSION["user"]->role == "admin") {
                            echo "<li><a href=\"/users.php\">Пользователи</a></li>";
                        } else {
                            echo "<li><a href=\"/books.php\">Книги</a></li>";
                        }
                    }
                ?>
                <li>
                    <?
                        if (isset($_SESSION["user"])) {
                            echo "<a href=\"/logout.php\">Выйти</a>";
                        } else {
                            echo "<a href=\"/login.php\">Войти</a>";
                        }
                    ?>
                </li>
            </ul>
        </header>
        <main>
            <form action="/change-password.php" method="POST">
                <?
                    if (isset($data["change_pass"])) {
                        if (empty($errors)) {
                            echo "<div>Вы успешно сменили пароль</div><hr>";
                        } else {
                            echo "<div>".$errors[0]."</div><hr>";
                        }
                    }
                ?>
                <table>
                    <tr>
                        <td>Старый пароль</td>
                        <td><input type="password" name="old_password" value="<?= $data["old_password"] ?>" autofocus></td>
                    </tr>
                    <tr>
                        <td>Новый пароль</td>
                        <td><input type="password" name="new_password"></td>
                    </tr>
                    <tr>
                        <td>Повторите пароль</td>
                        <td><input type="password" name="repeat_password"></td>
                    </tr>
                </table>
                <button type="submit" name="change_pass">Изменить</button>
            </form>
        </main>
    </body>
</html>
