<?
    require "db.php";
    require "functions.php";
    if (!isset($_SESSION["user"]) || $_SESSION["user"]->role == "admin") {
        header("Location: /");
    }
    $data = $_POST;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Библиотека</title>
        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/table.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="/js/books.js" defer></script>
    </head>
    <body>
        <header>
            <ul>
                <li><a href="/">Главная</a></li>
                <li><a href="/books.php">Книги</a></li>
                <li>
                    <?
                        if (isset($_SESSION["user"])) {
                            echo "<a href=\"/logout.php\">Выйти</a>";
                        } else {
                            echo "<a href=\"/login.php\">Войти</a>";
                        }
                    ?>
                </li>
            </ul>
        </header>
        <main>
            <h1>Книги</h1>
            <form action="/books.php" method="POST">
                <input name="name" placeholder="Название" value="<?= $data["name"] ?>">
                <input name="author" placeholder="Автор" value="<?= $data["author"] ?>">
                <input name="genre" placeholder="Жанр" value="<?= $data["genre"] ?>">
                <input name="publisher" placeholder="Издатель" value="<?= $data["publisher"] ?>">
                <button type="submit" name="search">Поиск</button>
            </form>
            <hr>
            <table>
                <tr>
                    <th>Название</th>
                    <th>Автор</th>
                    <th>Жанр</th>
                    <th>Издатель</th>
                    <?
                        if ($_SESSION["user"]->role == "librarian") {
                            echo "<th>Удалить</th>";
                            echo "<th>Забронировал</th>";
                            echo "<th>Бронь до</th>";
                            echo "<th>Выдана</th>";
                            echo "<th>Выдать</th>";
                            echo "<th>Принять</th>";
                        } else {
                            echo "<th>Бронь</th>";
                            echo "<th>Отмена</th>";
                            echo "<th>Бронь до</th>";
                        }
                    ?>
                </tr>
                <?
                    $name = trim($data["name"]);
                    $author = trim($data["author"]);
                    $genre = trim($data["genre"]);
                    $publisher = trim($data["publisher"]);
                    $books = R::find("books", "upper(name) LIKE upper(?) AND upper(author) LIKE upper(?) AND upper(genre) LIKE upper(?) AND upper(publisher) LIKE upper(?) ORDER BY name ASC", ["%".$name."%", "%".$author."%", "%".$genre."%", "%".$publisher."%"]);

                    if (count($books) > 0) {
                        if ($_SESSION["user"]->role == "librarian") {
                            foreach ($books as $book) {
                                if (isset($book["booked"])) {
                                    echo "<tr><td>".$book["name"]."</td><td>".$book["author"]."</td><td>".$book["genre"]."</td><td>".$book["publisher"]."</td><td><button class=\"remove-book\" data-name=\"".$book["name"]."\">Удалить</button></td><td>".$book["booked"]."</td><td>".date("H:i d.m.y", $book["book_date"])."</td><td></td><td><button class=\"give-book\" data-name=\"".$book["name"]."\">Выдать</button></td><td><button class=\"get-book\" data-name=\"".$book["name"]."\" disabled>Принять</button></td></tr>";
                                } else if (isset($book["given"])) {
                                    echo "<tr><td>".$book["name"]."</td><td>".$book["author"]."</td><td>".$book["genre"]."</td><td>".$book["publisher"]."</td><td><button class=\"remove-book\" data-name=\"".$book["name"]."\">Удалить</button></td><td></td><td></td><td>".$book["given"]."</td><td><button class=\"give-book\" data-name=\"".$book["name"]."\" disabled>Выдать</button></td><td><button class=\"get-book\" data-name=\"".$book["name"]."\">Принять</button></td></tr>";
                                } else {
                                    echo "<tr><td>".$book["name"]."</td><td>".$book["author"]."</td><td>".$book["genre"]."</td><td>".$book["publisher"]."</td><td><button class=\"remove-book\" data-name=\"".$book["name"]."\">Удалить</button></td><td></td><td></td><td></td><td><button class=\"give-book\" data-name=\"".$book["name"]."\" disabled>Выдать</button></td><td><button class=\"get-book\" data-name=\"".$book["name"]."\" disabled>Принять</button></td></tr>";
                                }
                            }
                        } else {
                            $books = array_filter($books, function($book) {
                                return (!isset($book["booked"]) || $book["booked"] == $_SESSION["user"]->login) && !isset($book["given"]);
                            });
                            if (count($books) > 0) {
                                foreach ($books as $book) {
                                    if (isset($book["booked"])) {
                                        echo "<tr><td>".$book["name"]."</td><td>".$book["author"]."</td><td>".$book["genre"]."</td><td>".$book["publisher"]."</td><td><button class=\"book-book\" data-name=\"".$book["name"]."\" disabled>Бронь</button></td><td><button class=\"cancel-book\" data-name=\"".$book["name"]."\">Отмена</button></td><td>".date("H:i d.m.y", $book["book_date"])."</td></tr>";
                                    } else {
                                        echo "<tr><td>".$book["name"]."</td><td>".$book["author"]."</td><td>".$book["genre"]."</td><td>".$book["publisher"]."</td><td><button class=\"book-book\" data-name=\"".$book["name"]."\">Бронь</button></td><td><button class=\"cancel-book\" data-name=\"".$book["name"]."\" disabled>Отмена</button></td><td></td></tr>";
                                    }
                                }
                            } else {
                                echo "<tr><td colspan=\"7\">Пусто</td></tr>";
                            }
                        }
                    } else {
                        if ($_SESSION["user"]->role == "librarian") {
                            echo "<tr><td colspan=\"10\">Пусто</td></tr>";
                        } else {
                            echo "<tr><td colspan=\"7\">Пусто</td></tr>";
                        }
                    }
                ?>
            </table>
            <hr>
            <?
                if ($_SESSION["user"]->role == "librarian") {
                    echo "<h3>Название: <input id=\"name\"> Автор: <input id=\"author\"> Жанр: <input id=\"genre\"> Издатель: <input id=\"publisher\"><button id=\"add-book\">Добавить</button></h3>";
                } else {
                    echo "<h1>Полученные мной</h1>";
                    $books = R::find("books", "given LIKE ? ORDER BY name ASC", [$_SESSION["user"]->login]);
                    echo "<table>";
                    echo "<tr><th>Название</th><th>Автор</th><th>Жанр</th><th>Издатель</th></tr>";
                    if (count($books) > 0) {
                        foreach ($books as $book) {
                            echo "<tr><td>".$book["name"]."</td><td>".$book["author"]."</td><td>".$book["genre"]."</td><td>".$book["publisher"]."</td></tr>";
                        }
                    } else {
                        echo "<tr><td colspan=\"4\">Пусто</td></tr>";
                    }
                    echo "</table>";
                }
            ?>
        </main>
    </body>
</html>
