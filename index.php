<?
    require "db.php";
    require "functions.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Библиотека</title>
        <link rel="stylesheet" href="/css/main.css">
    </head>
    <body>
        <header>
            <ul>
                <li><a href="/">Главная</a></li>
                <?
                    if (isset($_SESSION["user"])) {
                        if ($_SESSION["user"]->role == "admin") {
                            echo "<li><a href=\"/users.php\">Пользователи</a></li>";
                        } else {
                            echo "<li><a href=\"/books.php\">Книги</a></li>";
                        }
                    }
                ?>
                <li>
                    <?
                        if (isset($_SESSION["user"])) {
                            echo "<a href=\"/logout.php\">Выйти</a>";
                        } else {
                            echo "<a href=\"/login.php\">Войти</a>";
                        }
                    ?>
                </li>
            </ul>
        </header>
        <main>
            <?
                if (isset($_SESSION["user"])) {
                    echo "<b>Вы вошли как ".$_SESSION["user"]->login."</b>";
                    echo "<br>";
                    echo "<b>Ваша роль: ".roleFull($_SESSION["user"]->role)."</b>";
                    echo "<br>";
                    echo "<b style=\"color: #136fb3\"><a href=\"/change-password.php\">Изменить пароль</a></b>";
                } else {
                    echo "<b>Вы не вошли</b>";
                }
            ?>
        </main>
    </body>
</html>
