<?

function roleFull($role) {
    if ($role == "admin") {
        return "Администратор";
    } else if ($role == "librarian") {
        return "Библиотекарь";
    } else {
        return "Клиент";
    }
}
