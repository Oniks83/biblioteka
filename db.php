<?
require "rb.php";
require "config.php";

R::setup("pgsql:host=".DB_HOST.";dbname=".DB_NAME, DB_LOGIN, DB_PASSWORD);

$__admin__ = R::findOne("users", "login = ?", [ADMIN_LOGIN]);

if (!isset($__admin__)) {
    $user = R::dispense("users");
    $user->login = trim(ADMIN_LOGIN);
    $user->password = password_hash(ADMIN_PASSWORD, PASSWORD_DEFAULT);
    $user->role = "admin";
    R::store($user);
}

$__books__ = R::findAll("books");

foreach ($__books__ as $book) {
    if ($book->book_date < time()) {
        $book->booked = null;
        $book->book_date = null;
        R::store($book);
    }
}

if (TEST) {

    $__librarian__ = R::findOne("users", "login = ?", ["librarian"]);
    if (!isset($__librarian__)) {
        $user = R::dispense("users");
        $user->login = "librarian";
        $user->password = password_hash("asdfgh456", PASSWORD_DEFAULT);
        $user->role = "librarian";
        R::store($user);
    }

    $__client__ = R::findOne("users", "login = ?", ["client"]);
    if (!isset($__client__)) {
        $user = R::dispense("users");
        $user->login = "client";
        $user->password = password_hash("zxcvbn789", PASSWORD_DEFAULT);
        $user->role = "client";
        R::store($user);
    }

    $__name1__ = "Преступление и наказание";
    $__book1__ = R::findOne("books", "name = ?", [$__name1__]);
    if (!isset($__book1__)) {
        $book = R::dispense("books");
        $book->name = $__name1__;
        $book->author = "Фёдор Достоевский";
        $book->genre = "роман";
        $book->publisher = "Дрофа";
        $book->booked = null;
        $book->book_date = null;
        $book->given = null;
        R::store($book);
    }

    $__name2__ = "Гарри Поттер";
    $__book2__ = R::findOne("books", "name = ?", [$__name2__]);
    if (!isset($__book2__)) {
        $book = R::dispense("books");
        $book->name = $__name2__;
        $book->author = "Джоан Роулинг";
        $book->genre = "фантастика";
        $book->publisher = "Европа";
        $book->booked = null;
        $book->book_date = null;
        $book->given = null;
        R::store($book);
    }

    $__name3__ = "Поймай меня, если сможешь";
    $__book3__ = R::findOne("books", "name = ?", [$__name3__]);
    if (!isset($__book3__)) {
        $book = R::dispense("books");
        $book->name = $__name3__;
        $book->author = "Фрэнк Абигнейл";
        $book->genre = "автобиография";
        $book->publisher = "Эксмо";
        $book->booked = null;
        $book->book_date = null;
        $book->given = null;
        R::store($book);
    }

}

session_start();
