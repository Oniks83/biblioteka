<?
require "../db.php";

if ($_SESSION["user"]->role == "librarian") {

    $data = $_POST;
    $errors = [];
    $success = "false";

    $name = $data["name"];
    $author = $data["author"];
    $genre = $data["genre"];
    $publisher = $data["publisher"];
    if (isset($name) && isset($author) && isset($genre) && isset($publisher)) {
        $book = R::findOne("books", "name = ?", [$name]);
        if (isset($book)) {
            $errors[] = "Книга с таким названием уже существует";
        } else {
            $book = R::dispense("books");
            $book->name = $name;
            $book->author = $author;
            $book->genre = $genre;
            $book->publisher = $publisher;
            $book->booked = null;
            $book->book_date = null;
            $book->given = null;
            R::store($book);
        }
        if (empty($errors)) {
            $success = "true";
        }
    } else {
        $errors[] = "Недостаточно данных";
    }

    echo "{\"success\":".$success.",\"error\":\"".$errors[0]."\"}";

}
