<?
require "../db.php";

if ($_SESSION["user"]->role == "librarian") {

    $data = $_POST;
    $errors = [];
    $success = "false";

    $name = $data["name"];
    if (isset($name)) {
        $book = R::findOne("books", "name = ?", [$name]);
        if (isset($book)) {
            R::trash($book);
        } else {
            $errors[] = "Книга с таким названием не существует";
        }
        if (empty($errors)) {
            $success = "true";
        }
    } else {
        $errors[] = "Недостаточно данных";
    }

    echo "{\"success\":".$success.",\"error\":\"".$errors[0]."\"}";

}
