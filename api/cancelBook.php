<?
require "../db.php";

if ($_SESSION["user"]->role == "client") {

    $data = $_POST;
    $errors = [];
    $success = "false";

    $name = $data["name"];
    if (isset($name)) {
        $book = R::findOne("books", "name = ?", [$name]);
        if (isset($book)) {
            if ($book->booked == $_SESSION["user"]->login) {
                $book->booked = null;
                $book->book_date = null;
                R::store($book);
            } else {
                $errors[] = "Эту книгу не забронирована вами";
            }
        } else {
            $errors[] = "Книга с таким названием не существует";
        }
        if (empty($errors)) {
            $success = "true";
        }
    } else {
        $errors[] = "Недостаточно данных";
    }

    echo "{\"success\":".$success.",\"error\":\"".$errors[0]."\"}";

}
