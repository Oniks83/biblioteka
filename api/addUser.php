<?
require "../db.php";

if ($_SESSION["user"]->role == "admin") {

    $data = $_POST;
    $errors = [];
    $success = "false";

    $login = $data["login"];
    $password = $data["password"];
    $role = $data["role"];
    if (isset($login) && isset($password) && isset($role)) {
        $user = R::findOne("users", "login = ?", [$login]);
        if (isset($user)) {
            $errors[] = "Пользователь с таким логином уже существует";
        } else {
            $user = R::dispense("users");
            $user->login = $login;
            $user->password = password_hash($password, PASSWORD_DEFAULT);
            $user->role = $role;
            R::store($user);
        }
        if (empty($errors)) {
            $success = "true";
        }
    } else {
        $errors[] = "Недостаточно данных";
    }

    echo "{\"success\":".$success.",\"error\":\"".$errors[0]."\"}";

}
