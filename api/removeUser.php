<?
require "../db.php";

if ($_SESSION["user"]->role == "admin") {

    $data = $_POST;
    $errors = [];
    $success = "false";

    $login = $data["login"];
    if (isset($login)) {
        $user = R::findOne("users", "login = ?", [$login]);
        if (isset($user)) {
            R::trash($user);
        } else {
            $errors[] = "Пользователя с таким логином не существует";
        }
        if (empty($errors)) {
            $success = "true";
        }
    } else {
        $errors[] = "Недостаточно данных";
    }

    echo "{\"success\":".$success.",\"error\":\"".$errors[0]."\"}";

}
