<?
    require "db.php";
    require "functions.php";
    if (!isset($_SESSION["user"]) || $_SESSION["user"]->role != "admin") {
        header("Location: /");
    }
    $data = $_POST;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Библиотека</title>
        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/table.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="/js/users.js" defer></script>
    </head>
    <body>
        <header>
            <ul>
                <li><a href="/">Главная</a></li>
                <li><a href="/users.php">Пользователи</a></li>
                <li>
                    <?
                        if (isset($_SESSION["user"])) {
                            echo "<a href=\"/logout.php\">Выйти</a>";
                        } else {
                            echo "<a href=\"/login.php\">Войти</a>";
                        }
                    ?>
                </li>
            </ul>
        </header>
        <main>
            <h1>Пользователи</h1>
            <table>
                <tr>
                    <th>Логин</th>
                    <th>Роль</th>
                    <th>Удалить</th>
                </tr>
                <?
                    $users = R::findAll("users", "ORDER BY login ASC");
                    foreach ($users as $user) {
                        if ($_SESSION["user"]->login == $user["login"]) {
                            echo "<tr><td>".$user["login"]."</td><td>".roleFull($user["role"])."</td><td></td></tr>";
                        } else {
                            echo "<tr><td>".$user["login"]."</td><td>".roleFull($user["role"])."</td><td><button class=\"remove-user\" data-login=\"".$user["login"]."\">Удалить</button></td></tr>";
                        }
                    }
                ?>
            </table>
            <hr>
            <h3>Логин: <input type="text" id="login"> Пароль: <input type="password" id="password"> Роль: <select id="role"><option value="client"><?= roleFull("client") ?></option><option value="librarian"><?= roleFull("librarian") ?></option><option value="admin"><?= roleFull("admin") ?></option></select><button id="add-user">Добавить</button></h3>
        </main>
    </body>
</html>
